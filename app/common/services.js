angular.module('myApp')
.service('candidateService',['$resource', function ($resource) {
	var candidateResource = $resource('http://localhost:3000/records',{},{
				getData: {
			      method: "GET",
			      isArray:true
			    },
			    sendData: {
			      method: "POST"
			    }
			});
	this.getData=function(){
        	return 	candidateResource.getData().$promise;
    };
    this.sendData=function(data){
    	return candidateResource.sendData(data).$promise;
    };
}])
.service('dropDownService',['$resource', '$q', function ($resource, $q) {
	var data;
	var self = this;
	var deffer = $q.defer();

	this.get = function(){
		if(!data){
			self.getDropDowndata().get(function(d){
				data = d;
				deffer.resolve(data);
			}, function(){
				deffer.reject();
			});
		}else{
			deffer.resolve(data);
		}
		return deffer.promise;
	}
	this.getDropDowndata=function(){
	    	return $resource('http://localhost:3000/data', {});
	    }
}]);