'use strict';
angular.module('myApp', [
  'ui.router',
  'ngResource',
  'myApp.version'
])
.config(['$urlRouterProvider','$stateProvider', function($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/viewDashboard');
    $stateProvider.state('viewDashboard', {
        url: '/viewDashboard',
        templateUrl: '../viewDashboard/viewDashboard.html',
        controllerAs: '',
        controller: ''
    });
    $stateProvider.state('viewAboutus', {
        url: '/viewAboutus',
        templateUrl: '../viewAboutus/viewAboutus.html',
        controllerAs: '',
        controller: ''
    });
    $stateProvider.state('viewForm', {
        url: '/viewForm',
        templateUrl: '../viewForm/viewForm.html',
        controllerAs: 'form',
        controller: 'formController'
    });
    $stateProvider.state('viewReport', {
        url: '/viewReport',
        templateUrl: '../viewReport/viewReport.html',
        controllerAs: 'report',
        controller: 'reportController'
    });
}]);
