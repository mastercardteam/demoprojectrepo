describe('reportController', function() {

    var $rootScope, $scope, $controller,reportController, $httpBackend;

    beforeEach(module('myApp'));
    beforeEach(inject(function($controller){
      reportController = $controller('reportController');

      describe('Controller name', function() {
        it('should exist', function() {
            expect(reportController).toBeDefined();
        });
     }); 
      
       it("Should return some value",(inject(function ($rootScope, $controller, candidateService) {
        var scope = $rootScope.$new();
        var controller = $controller("reportController", { $scope: scope, candidateService: candidateService });
        controller.getData();
        expect(scope.candidateInfo).not.toBe(null);
    })));

    beforeEach(inject(function (_$httpBackend_, candidateService) {
      $httpBackend = _$httpBackend_;
      candidateService = _candidateService_;
    }));

    describe('getData', function () {
        it('should call getData to retrive data from backend', inject(function (candidateService) {
            $httpBackend.expectGET('http://localhost:3000/records').respond('testing data');
            candidateService.getData().then(function(data) {
                expect(data).toEqual('testing data');
            });
            $httpBackend.flush();
        }));
    });
    }));
});