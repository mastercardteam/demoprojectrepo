'use strict';
var myApp = angular.module('myApp')
.controller('formController',['candidateService','dropDownService','$scope','$q',
    function(candidateService,dropDownService,$scope,$q){
    var self=this;
    function getSkillSet() {
        var deferred = $q.defer();
         dropDownService.get().then(function(response){
            self.skillSet=response.skillSet;
            deferred.resolve(self.skillSet);
         },function(error) {
            console.log(error);
            deferred.reject(error);
        });
         return deferred.promise;
    }
     function getExperience() {
        var deferred = $q.defer();
         dropDownService.get().then(function(response){
            self.expData=response.exp;
             deferred.resolve(self.expData);
         },function(error) {
            console.log(error);
            deferred.reject(error);
        });
    }
     function getGender() {
        var deferred = $q.defer();
        dropDownService.get().then(function(response){
            self.genderData=response.gender;
              deferred.resolve(self.genderData);
         },function(error) {
            console.log(error);
             deferred.reject(error);
        });
    }
    let promises = [getSkillSet(),getExperience(),getGender()];
        $q.all(promises).then(function(){
            });
	$scope.submitCandidateForm=function(candidate2){
        var data=candidate2; 
         candidateService.sendData(data).then(function(response){
                console.log("Data fetched");
         },function(error){
                console.log(error);
         });
        }
}]);
